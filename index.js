/* 

		fetch() is a method is JS, which allows to send request to an api and process its response, fetch has 2 agruments,the url to resource/route and optional object which cotains additional infomation about our request such as method,the body and the headers of our request



		fetch()method

		Syntaxkey: "value", 
		fetch(url, options )


*/



//get posts data using fetch()

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => console.log(data))


//add post data

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1

		}),
		headers: {
			'Content-type': 'application/json; charset=UTF-8'
		}
	})
	.then((response) => response.json())
	.then((data) => {

		console.log(data);
		alert("Successfully Added.");


		//Clear the text elements upon post creation
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;


	})	


	// END OF REQUEST 
	const showPosts = (posts) => {
	let postEntries = ''

	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}



//mini activity get retrieve all the post





//edit post

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML


	document.querySelector('#txt-edit-id').value = id
	document.querySelector('#txt-edit-title').value = title
	document.querySelector('#txt-edit-body').value = body


	//removes the disabled attribute from the button 
	document.querySelector('#btn-submit-update').removeAttribute('disabled') 
}

//update post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault()


	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({

			id:document.querySelector('#txt-edit-id').value, 
			title:document.querySelector('#txt-edit-title').value, 
			body:document.querySelector('#txt-edit-body').value,
			userId: 1 
		}),

		headers: {
			'Content-type': 'application/json; charset=UTF-8'
		}
	})



	.then((response) => response.json())
	.then((data) => {

		console.log(data)
		alert('Succesfully updated')


		document.querySelector('#txt-edit-id').value = null
		document.querySelector('#txt-edit-title').value = null
		document.querySelector('#txt-edit-body').value = null


		document.querySelector('#btn-submit-update').setAttribute('disabled', true)
	})
})



fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => showPosts(data))





const deletePost = (id) => {


	document.querySelector(`#post-${id}`).remove()

	console.log(posts)
}
